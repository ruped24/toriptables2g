# toriptables2g with GUI Desktop notification  

![](https://img.shields.io/badge/toriptables2g-python%202.7-blue.svg?style=flat-square) ![](https://img.shields.io/badge/dependencies-tor_-orange.svg?style=flat-square) ![](https://img.shields.io/badge/GPL-v2-blue.svg?style=flat-square) ![GitHub stars](https://img.shields.io/github/stars/ruped24/toriptables2.svg?style=social)

Tor Iptables script is an anonymizer that sets up iptables and tor to route all services and traffic including DNS through the Tor network.
***

### Dependencies:

`sudo apt install tor`

### [Usage](https://drive.google.com/file/d/0B79r4wTVj-CZSEdkaTBNOVc5aUU/view): ###

```
#!bash

toriptables2g.py -h
```
### Screenshots: ###

* [Kali Linux, Rolling Edition](https://drive.google.com/file/d/0B79r4wTVj-CZSEdkaTBNOVc5aUU/view)

* [Kali Linux, Rolling Edition](https://drive.google.com/file/d/1NPZ1SKI9rh9XkaplF2_DzHSoPL77aiy-/view?usp=sharing)

* [Tor IPTables rules loaded](https://drive.google.com/open?id=0B79r4wTVj-CZT0NMV2VZRTM1REE)


### To test: ###

* [Check My IP](http://check-my-ip.net)

* [Check Tor Project](https://check.torproject.org)
 
* [Witch proxy checker](http://witch.valdikss.org.ru)

* [Do I leak](http://www.doileak.com/)
 
* [DNS leak test](http://dnsleaktest.com)

* [What every Browser knows about you](http://webkay.robinlinus.com/)

### To manually change IP w/o reload:
Refresh Check Tor Project webpage

```
#!bash

sudo kill -HUP $(pidof tor)
```

### To automate changing Tor IP: ###

* [Screenshot](https://drive.google.com/open?id=0B79r4wTVj-CZdUtGU3p6WldHX2s)


* [tor_manager](https://bitbucket.org/ruped24/tor_manager/src)

****
### [Installation](https://github.com/ruped24/toriptables2/wiki/Optional-Installation-methods-for-toriptables2.py)

### [Troubleshooting](https://github.com/ruped24/toriptables2/wiki/Troubleshooting)
****